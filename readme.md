# Design Challenge

## Instructions

Our client's website needs a makeover! Help them improve their site by designing their User Profile page based on the [wireframe](https://goo.gl/WatULV) they provided. They also prepared a collection of screenshots for you to [warm up your creative juice](https://goo.gl/Tnqt4C).

Click here to read the full instructions: 
[Design Challenge Instructions](https://goo.gl/RgXJAo)